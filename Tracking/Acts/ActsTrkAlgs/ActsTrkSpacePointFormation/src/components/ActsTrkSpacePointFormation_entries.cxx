/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "src/PixelSpacePointFormationAlg.h"
#include "src/StripSpacePointFormationAlg.h"

DECLARE_COMPONENT( ActsTrk::PixelSpacePointFormationAlg )
DECLARE_COMPONENT( ActsTrk::StripSpacePointFormationAlg )
