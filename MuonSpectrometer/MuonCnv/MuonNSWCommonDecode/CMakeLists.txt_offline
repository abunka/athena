# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( MuonNSWCommonDecode )

# Use tdaq-common packages
find_package( tdaq-common COMPONENTS DataReader EventStorage eformat ers)
find_package( Boost )
find_package( ROOT REQUIRED COMPONENTS Core Hist Tree RIO Gpad Graf )

# Component(s) in the package:
atlas_add_library (MuonNSWCommonDecode
		   src/*.cxx src/*.cpp
                   PUBLIC_HEADERS MuonNSWCommonDecode
                   INCLUDE_DIRS ${TDAQ-COMMON_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
                   LINK_LIBRARIES ${TDAQ-COMMON_LIBRARIES} ${Boost_LIBRARIES})

atlas_add_executable (test_stgc_cabling
		      src/test/test_stgc_cabling.cxx
          INCLUDE_DIRS ${TDAQ-COMMON_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
		      LINK_LIBRARIES MuonNSWCommonDecode ${TDAQ-COMMON_LIBRARIES} ${Boost_LIBRARIES})

atlas_add_executable (test_nsw_common_decoder
		      src/test/test_nsw_common_decoder.cxx
          INCLUDE_DIRS ${TDAQ-COMMON_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
		      LINK_LIBRARIES MuonNSWCommonDecode ${TDAQ-COMMON_LIBRARIES} ${Boost_LIBRARIES})

atlas_add_executable (test_nsw_trigger_common_decoder
		      src/test/test_nsw_trigger_common_decoder.cxx
          INCLUDE_DIRS ${TDAQ-COMMON_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
		      LINK_LIBRARIES MuonNSWCommonDecode ${TDAQ-COMMON_LIBRARIES} ${Boost_LIBRARIES} ${ROOT_LIBRARIES})

atlas_add_executable (test_nsw_online2offline
		      src/test/test_nsw_online2offline.cxx
          INCLUDE_DIRS ${TDAQ-COMMON_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
		      LINK_LIBRARIES MuonNSWCommonDecode ${TDAQ-COMMON_LIBRARIES} ${Boost_LIBRARIES})
